<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <?php 
  if ((!empty($_GET))  && (array_key_exists('name', $_GET))) {
    $title = "Тест {$_GET['name']}";
  } else $title = "Тест не выбран";
   ?>
  <title><?=$title?></title>
</head>
<body>
<?php 
if ((!empty($_GET))  && (array_key_exists('name', $_GET))) {
  $testname =$_GET['name'];
  $file = file_get_contents("./tests/$testname");
  if (!$file) {
    header("HTTP/1.0 404 Not Found");
    exit;
  }
  $array = json_decode($file, TRUE);
 ?>
 <form action="test.php" method="POST">
  <fieldset>
    <legend><label for="username">Ваше имя:</label>
    <input id="username" type="text" name="username" required><br></legend>
    <?php 
    $i = 0;
    foreach ($array as $value) {      
      shuffle($value['answers']); 
      echo "<legend>{$value['question']}</legend>";
      foreach ($value['answers'] as $questions) {
        echo '<label><input type="radio" value="'.$questions.'" required name="q'.$i.'"> '.$questions.'</label>';
      }
      $i++;
    }
    echo '<input type="hidden" value="'.$testname.'" name="numbertest">';
     ?>
  </fieldset>
  <input type="submit" value="Отправить">  
  </form>
  <a href="admin.php">Загрузить еще тесты</a><br>
  <a href="list.php">к списку тестов</a><br>
  <?php 

  } elseif (!empty($_POST)) {
  $testname = $_POST['numbertest'];
  $file = file_get_contents("./tests/$testname");
  $array = json_decode($file, TRUE);
  $i = 0;
  $username = $_POST['username'];
  unset($_POST['username']);
  echo "Привет, $username <br>";
  $true = 0;
  $question = 0;
  foreach ($_POST as $key => $value) {
    if ($key !== 'numbertest') {
      if ($value === $array[$i]['trueAnswer']) {
        echo "{$array[$i]['question']} <br> Ваш ответ: $value, Правильно! <br><br>"; 
        $true++;
        $question++;
      } else echo "{$array[$i]['question']} <br> Ваш ответ: $value, Неправильно! <br> Правильный ответ: {$array[$i]['trueAnswer']}<br><br>";
        $question++;
    }
    $i++;
  }
$result = ($true/$question)*2*100;

echo '<img src="./image.php?username='.$username.'&result='.$result.'" alt="sertificate"><br>';
?>
  <a href="admin.php">Загрузить еще тесты</a><br>
  <a href="list.php">к списку тестов</a><br>
<?php
echo '<a href="test.php?name='.$testname.'">Этот же тест заново</a>';
  }
else {
  header("HTTP/1.0 404 Not Found");
  echo "<h3>Тест не найден</h3>";
  echo '<a href="list.php">к списку тестов</a><br>';
  exit;
}
?>


</body>
</html>