 <!DOCTYPE html>
 <html lang="en">
 <head>
 	<meta charset="UTF-8">
 	<title>Загрузить тесты</title>
 </head>
 <body>
 <form action="admin.php" method="POST" enctype="multipart/form-data">
	<input type="file" name="test">
	<input type="submit">
</form>
<?php 
	if ((!empty($_FILES)) && (is_uploaded_file($_FILES['test']['tmp_name']))) {
		if ($_FILES['test']['type'] === 'application/json') {
			$upload = move_uploaded_file($_FILES['test']['tmp_name'], "./tests/{$_FILES['test']['name']}");

			if ($upload) {
				echo "Файл загружен!";
				header('Location: ./list.php');
				exit;
			}
			
		}
		else echo 'Неверный формат файла!';
	}

 ?>


<a href="list.php">к списку тестов</a>
 	
 </body>
 </html>